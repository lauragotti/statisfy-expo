(en mode developpement il faut ajouter dans son dashboard spotify l'email des utilisateurs autorisés à se connecter, pour tester faut me donner son e-mail)

Voir démo de l'appli dans /screenshots

# Lancer avec pnpm start :-)

Fait avec l'api / oauth de spotify, pour afficher des statistiques (top 10 des artistes, musiques et genres les plus écoutés du dernier mois de l'utilisateur connecté).

J'ai utilisé le implicit grant flow pour l'authentification sur spotify, ça renvoit un access_token qui dure 1h.
Le token est stocké sur async storage.

- Le download image fonctionne uniquement sur la version web car sur expo go on peut pas installer les modules necessaires (react-native-view-shot) j'ai installé html-to-image qui marche sur la version web
- Je voulais ajouter la fonctionnalité partager sur instagram mais sur expo go on peut pas non plus installer le module react-native-share
- Pareil le top genre marche pas sur expo go (react-chartjs-2), j'avais fait avec react-native-chart-kit mais c'etait moins beau mais du coup ça peut pas se lancer sur expo go du tout s'il est encore dans la config

## Compiler l'application pour Android

> En local, avec WSL

1. Installer Java 17

```sh
sudo apt install openjdk-17-jdk openjdk-17-jre

# Pour vérifier la version

java -version
```

2. Installer les outils de CLI pour Android

Les instructions viennent d'ici : https://developer.android.com/tools/sdkmanager.

```sh
# S'assurer d'avoir le lien à jour depuis https://developer.android.com/studio#command-line-tools-only
wget https://dl.google.com/android/repository/commandlinetools-linux-11076708_latest.zip
unzip commandlinetools-linux-11076708_latest.zip -d android
cd android/cmdline-tools
mkdir latest
mv bin lib source.properties NOTICE.txt -t latest
```

3. Exposer les outils

```sh
# Dans son fichier .zshrc ou autre
export ANDROID_HOME="$HOME/android"
export ANDROID_TOOLS="$ANDROID_HOME/cmdline-tools/latest/bin"
export ANDROID_EMULATOR="$ANDROID_HOME/emulator"
export ANDROID_PLATFORM_TOOLS="$ANDROID_HOME/platform-tools"
export PATH="$ANDROID_TOOLS:$ANDROID_EMULATOR:$ANDROID_PLATFORM_TOOLS:$PATH"
```

4. Installer la SDK Android et l'émulateur

Prendre la dernière version d'Android, supportée par la version d'Expo utilisée.

```sh
sdkmanager "platform-tools" "platforms;android-35" "system-images;android-35;google_apis;x86_64"
```

5. Créer un émulateur

```sh
avdmanager create avd -n android-35-emulator -k "system-images;android-35;google_apis;x86_64" -d pixel
```

6. Lancer l'émulateur

D'abord, il faut s'assurer d'avoir la virtualisation activée et fonctionnelle : https://developer.android.com/studio/run/emulator-acceleration#vm-linux.

```sh
emulator @android-35-emulator
```

> Même si la virtualisation est fonctionnelle, il peut être nécessaire de lancer
> cette commande pour s'assurer que l'utilisateur y ait bien accès :
>
> ```sh
> sudo chown <username> -R /dev/kvm
> ```

7. Compiler l'application

```sh
# En mode debug par défaut
pnpm android
# En mode production
pnpm android --variant release
```

La commande va automatiquement lancer l'émulateur (si un seul est créé), compiler l'application, et l'installer sur l'émulateur.

L'APK produit sera stocké dans `/android/app/build/outputs/apk/<variant>/apk-<variant>.apk`. L'artéfact peut être ensuite distribué à d'autres émulateurs (ex : Android Studio sur Windows), ou de vrais appareils.
