export interface responseTopArtists {
    href: string,
    items: Artist[],
    limit: number,
    next: string,
    offset: number,
    previous: string,
    total: number
  }
  
 export interface Artist {
    external_urls?: {},
    followers: {},
    genres: string[],
    href: string,
    id: string,
    images: [{}],
    name: string,
    popularity: number,
    type: string,
    uri: string,
  }

  export interface User {
    country:string,
    display_name:string,
    email:string,
    explicit_content:{},
    external_urls:{},
    followers:{},
    href:string,
    id:string,
    images: [{}],
    product:string,
    type:string,
    uri:string
  }

  export interface responseTopTracks {
    href: string,
    items: Track[],
    limit: number,
    next: string,
    offset: number,
    previous: string,
    total: number
  }

  export interface Track {
    album:{},
    artists:[{
      external_urls:{},
      href:string,
      id:string,
      name:string,
      type:string,
      uri:String
    }],
    available_market:[],
    disc_number:number,
    duration_ms:number,
    explicit:boolean,
    external_ids:{},
    external_urls:{},
    href:string,
    id:string,
    is_local:boolean,
    name:string,
    popularity:number,
    preview_url:string,
    track_number:number,
    type:string,
    uri:string
  }

  
  export interface DataItem {
    label:string,
    value:number
  }