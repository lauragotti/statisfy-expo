import { Pressable, View, Text, StyleSheet } from "react-native";

interface Props {
    onPress: () => void
}
export default function DownloadButton({ onPress }: Props) {
    return (
        <View style={styles.btnContainer}>
            <Pressable style={styles.buttonDownload} onPress={onPress}>
                <Text style={styles.buttonsText}>download image</Text>
            </Pressable>
        </View>
    )
}

const styles = StyleSheet.create({
    btnContainer: {
        margin: 5
    },
    buttonDownload: {
        backgroundColor: '#00dbff',
        padding: 12,
        borderRadius: 20
    },
    buttonsText: {
        color: 'white',
        textTransform: 'uppercase'
    }
})
