import * as React from 'react';
import * as WebBrowser from 'expo-web-browser';
import { makeRedirectUri, ResponseType, TokenResponse, useAuthRequest } from 'expo-auth-session';
import { Button, View, Text, StyleSheet, ImageBackground, PanResponder, Pressable } from 'react-native';
import { useEffect, useRef, useState } from 'react';
import axios from 'axios';
import { Artist, DataItem, Track, User } from '../entities';
import { fetchTopArtists, fetchTopTracks, fetchUserInfo } from '../service';
import * as htmlToImage from 'html-to-image';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useFonts, ShadowsIntoLight_400Regular } from '@expo-google-fonts/shadows-into-light';
import { RobotoSlab_300Light } from '@expo-google-fonts/roboto-slab'
import { Pie } from 'react-chartjs-2';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { SafeAreaView } from 'react-native-safe-area-context';
import DownloadButton from '../components/DownloadButton';

ChartJS.register(ArcElement, Tooltip, Legend);

WebBrowser.maybeCompleteAuthSession();

// Endpoint spotify
const discovery = {
  authorizationEndpoint: 'https://accounts.spotify.com/authorize',
  tokenEndpoint: 'https://accounts.spotify.com/api/token',
};

export default function StatisfyScreen() {

  let [fontsLoaded] = useFonts({
    ShadowsIntoLight_400Regular,
    RobotoSlab_300Light
  });

  //var user
  const [user, setUser] = useState<User>()

  //auth
  const [token, setToken] = useState('');
  useEffect(() => {
    const getToken = async () => {
      try {
        const value = await AsyncStorage.getItem('token');
        if (value !== null) {
          setToken(value);
        }
      } catch (e) {
      }
    };
    getToken();
  }, []);

  const [request, response, promptAsync] = useAuthRequest(
    {
      responseType: ResponseType.Token,
      clientId: 'a5b15fd854b042839f9166833bc4771b', //faudrait le cacher du code
      scopes: [
        "user-read-currently-playing",
        "user-read-recently-played",
        "user-read-playback-state",
        "user-top-read",
        "user-modify-playback-state",
        "streaming",
        "user-read-email",
        "user-read-private",
      ],
      usePKCE: false,
      redirectUri: makeRedirectUri(),
    },
    discovery
  );

  useEffect(() => {
    if (response?.type === 'success') {
      const { access_token } = response.params;
      setToken(access_token);
      const storeData = async (access_token: string) => {
        try {
          await AsyncStorage.setItem('token', access_token)
        } catch (error) {
          console.log('error')
        }
      }
      storeData(access_token);
    } else if (response?.type === 'error') {
      console.log("Error:", response.error);
    }
    else if (response?.type === 'dismiss') {
      console.log("Dismissed");
    }
    else if (response?.type === 'cancel') {
      console.log("Cancelled");
    }
  }, [response]);

  /**
   * pour tester que le token ai pas expiré, utilisé pour l'affichage VRAIMENT PAS SURE QUE CE SOIT BIEN
   * @param token 
   * @returns true/false token valide ou non
   */
  async function validToken(token: string) {
    try {
      const response = await axios.get("https://api.spotify.com/v1/me", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.status === 200) {
        return true;
      } else {
        console.log("no valid token");
        return false;
      }
    } catch (error: any) {
      console.log("no valid token");
      return false;
    }
  }
  const [isTokenValid, setIsTokenValid] = useState(false);

  useEffect(() => {
    async function checkToken() {
      const result = await validToken(token);
      setIsTokenValid(result);
    }
    checkToken();
  }, [token]);

  //top artists
  const [topArtists, setTopArtists] = useState<Artist[]>([]);
  const [showTA, setShowTA] = useState<Boolean>(false);
  /**
   * fetch top 10 artists medium term
   */
  async function getTopArtists() {
    const data = await fetchTopArtists(token);
    setTopArtists(data.items);
    setShowTA(true);
    setShowTT(false)
    setShowGenres(false)
  }

  //top tracks
  const [topTracks, setTopTracks] = useState<Track[]>([]);
  const [showTT, setShowTT] = useState<Boolean>(false)
  /**
   * fetch top 10 tracks medium term
   */
  async function getTopTracks() {
    const data = await fetchTopTracks(token);
    setTopTracks(data.items)
    setShowTT(true)
    setShowTA(false)
    setShowGenres(false)
  }

  /**
   * fetch infos utilisateur 
   */
  async function getUserInfo() {
    try {
      const data = await fetchUserInfo(token);
      console.log(data);
      setUser(data);
    } catch (error) {
      console.error(error);
    }
  }
  useEffect(() => {
    if (token) {
      getUserInfo();
    }
  }, [token]);

  /**
   * logout : vide le token, reset les top cachés
   */
  function logout() {
    setToken('');
    setShowTA(false)
    setShowTT(false)
    setShowGenres(false)
  }

  //download image
  const ref = useRef(null)
  /**
   * recupere la ref et transforme en png avec html-to-image et ouvre fenetre de sauvegarde de l'image, pour top artists
   */
  const downloadImage = async () => {
    if (ref.current) {
      const dataUrl = await htmlToImage.toPng(ref.current)
      const link = document.createElement('a');
      link.download = "top-artists.png";
      link.href = dataUrl;
      link.click();
    }
  }

  const tracksref = useRef(null)
  /**
 * recupere la ref et transforme en png avec html-to-image et ouvre fenetre de sauvegarde de l'image, pour top tracks
 */
  const downloadImageTracks = async () => {
    if (tracksref.current) {
      const dataUrl = await htmlToImage.toPng(tracksref.current)
      const link = document.createElement('a');
      link.download = "top-tracks.png";
      link.href = dataUrl;
      link.click();
    }
  }

  const pieref = useRef(null)
  /**
   * recupere la ref et transforme en png avec html-to-image et ouvre fenetre de sauvegarde de l'image, pour top genres
   */
  const downloadImagePie = async () => {
    if (pieref.current) {
      const dataUrl = await htmlToImage.toPng(pieref.current)
      const link = document.createElement('a');
      link.download = "top-genre.png";
      link.href = dataUrl;
      link.click();
    }
  }

  //var backgrounds 
  const bg = require('../assets/black-statisfy.png')
  const bgtracks = require('../assets/statisfy-tracks.png')

  //genres
  const [genres, setGenres] = useState<string[]>([])
  const [showGenres, setShowGenres] = useState<Boolean>(false)
  /**
   * fetch les top artists, boucle dessus, boucle dans leurs genre et les push dans un tableau
   */
  async function getGenres() {
    const data = await fetchTopArtists(token)
    const allGenres: any = []
    data.items.map(item => {
      item.genres.map(genre => {
        allGenres.push(genre)
      })
    })
    setGenres(allGenres)
    setShowGenres(true)
    setShowTA(false)
    setShowTT(false)
  }

  const [genresCount, setGenresCount] = useState<DataItem[]>([])
  //map sur le tableau de genre et compte le nombre de repetition d'un genre, push dans un tableau d'objet qui a la key(genre) et la value(nombre de repetition)
  useEffect(() => {
    const array = genres;
    const counts: any = {};
    array.map((value) => {
      counts[value] = (counts[value] || 0) + 1;
    })
    const data = Object.keys(counts).map((key) => ({
      label: key,
      value: counts[key],
    }))
    setGenresCount(data)
  }, [genres])

  //données pour le graphique, prend les 10 premiers items de genresCount
  const dataPie = {
    labels: genresCount.sort((a, b) => b.value - a.value).slice(0, 10).map((item) => item.label),
    datasets: [
      {
        data: genresCount.slice(0, 10).map((item) => item.value),
        backgroundColor: [
          '#35e57b',
          '#00eac9',
          '#00e7ff',
          '#00dbff',
          '#97c4ff',
          '#ffa5ff',
          '#ff8aea',
          '#ff8b9e',
          '#ffa65b',
          '#ebc42f',
        ],
        borderColor: [
          '#35e57b',
          '#00eac9',
          '#00e7ff',
          '#00dbff',
          '#97c4ff',
          '#ffa5ff',
          '#ff8aea',
          '#ff8b9e',
          '#ffa65b',
          '#ebc42f',
        ]
      }
    ]
  }

  return (
    <>
      <SafeAreaView style={styles.container}>
        {!isTokenValid ?
          <View style={styles.loginContainer}>

            <Pressable disabled={!request}
              onPress={() => {
                promptAsync()
              }} style={styles.buttonLogin}>
              <Text style={styles.buttonsText}>login with spotify</Text>
            </Pressable>
          </View>
          : null
        }
        {!showTA && isTokenValid ?
          <>
            <View style={styles.btnContainer}>
              <Pressable style={styles.buttonOne}
                onPress={() => {
                  getTopArtists()
                }}
              >
                <Text style={styles.buttonsText}>top artists from last month</Text>
              </Pressable>
            </View>
          </>
          : null
        }
        {!showTT && isTokenValid ?
          <View style={styles.btnContainer}>
            <Pressable style={styles.buttonTwo}
              onPress={() => {
                getTopTracks()
              }}>
              <Text style={styles.buttonsText}>top tracks from last month</Text>
            </Pressable>
          </View> : null}
        {!showGenres && isTokenValid ?
          <View style={styles.btnContainer}>
            <Pressable style={styles.buttonThree}
              onPress={() => {
                getGenres()
              }}
            >
              <Text style={styles.buttonsText}>top genres from last month</Text>
            </Pressable>
          </View>
          : null}
        {showTA && isTokenValid ?
          <>
            <View style={styles.artistsResult} >
              <ImageBackground source={bg} resizeMode="stretch" style={styles.background} ref={ref}>
                <Text style={styles.nameFest}>{user?.display_name}'s fest{'\n'}</Text>
                {topArtists.slice(0, 3).map((item) => (
                  <Text key={item.id} style={styles.firsts}>{item.name}{'\n'}</Text>
                ))}
                {topArtists.slice(3).map((item) => (
                  <Text key={item.id} style={styles.lasts}>{item.name}{'\n'}</Text>
                ))}
              </ImageBackground>
            </View>
            <DownloadButton onPress={downloadImage} />
          </> : null}
        {showTT && isTokenValid ?
          <>
            <View style={styles.tracksResults}>
              <ImageBackground source={bgtracks} resizeMode="stretch" ref={tracksref} style={styles.bgTracks}>
                <Text style={styles.tracks} >
                  {topTracks.map((item) =>
                    <Text key={item.id}>{item.name} - {item.artists[0].name}{'\n'}</Text>
                  )}
                </Text>
              </ImageBackground>
            </View>
            <DownloadButton onPress={downloadImageTracks} />

          </>
          : null}
        {showGenres && isTokenValid ?
          <>
            <View ref={pieref} style={{ backgroundColor: 'white', padding: 15 }}>
              <Pie data={dataPie} />
            </View>
            <DownloadButton onPress={downloadImagePie} />
          </>
          : null}
        {isTokenValid ?
          <>
            <Text style={styles.logged}>Logged as {user?.display_name}</Text>
            <View style={styles.btnContainer}>
              <Pressable onPress={logout}>
                <Text style={styles.logoutBtn}>Logout</Text>
              </Pressable>
            </View>
          </>
          : null}
      </SafeAreaView>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingTop: 10
  },
  artistsResult: {
    backgroundColor: '#000',
    marginTop: 20,
    marginBottom: 20,
    width: 300,
    height: 400
  },
  firsts: {
    fontWeight: 'bold',
    fontSize: 24,
    lineHeight: 42,
    textTransform: 'uppercase',
    textAlign: 'center',
    fontFamily: 'ShadowsIntoLight_400Regular',
    color: 'white'

  },
  lasts: {
    lineHeight: 30,
    textTransform: 'uppercase',
    textAlign: 'center',
    fontFamily: 'ShadowsIntoLight_400Regular',
    fontSize: 19,
    color: 'white'
  },
  background: {
    flex: 1,
    justifyContent: 'center',
    padding: 10,
    width: 300,
    height: 400
  },
  logged: {
    textAlign: 'center',
    marginTop: 20
  },
  nameFest: {
    textTransform: 'uppercase',
    textAlign: 'center',
    fontFamily: 'ShadowsIntoLight_400Regular',
    color: 'white'
  },
  btnContainer: {
    margin: 5
  },
  tracksResults: {
    width: 300,
    height: 300,
    marginTop: 20,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tracks: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'RobotoSlab_300Light'
  },
  bgTracks: {
    width: 300,
    height: 300,
    padding: 20
  },
  logoutBtn: {
    color: 'red',
    textDecorationLine: 'underline',
    marginBottom: 20
  },
  loginContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 10
  },
  buttonOne: {
    backgroundColor: '#d5a5e2',
    padding: 12,
    borderRadius: 20
  },
  buttonsText: {
    color: 'white',
    textTransform: 'uppercase'
  },
  buttonTwo: {
    backgroundColor: '#ff8aea',
    padding: 12,
    borderRadius: 20
  },
  buttonThree: {
    backgroundColor: '#ff8b9e',
    padding: 12,
    borderRadius: 20
  },
  buttonDownload: {
    backgroundColor: '#00dbff',
    padding: 12,
    borderRadius: 20
  },
  buttonLogin: {
    backgroundColor: '#1ccc5b',
    padding: 12,
    borderRadius: 20
  }
})


