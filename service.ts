import axios from "axios";
import { User, responseTopArtists, responseTopTracks } from "./entities";

export async function fetchTopArtists(token:string) {
    const response =  await axios.get<responseTopArtists>(
        "https://api.spotify.com/v1/me/top/artists?limit=10&offset=0",
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      return response.data
}

export async function fetchUserInfo(token:string) {
    const response = await axios.get<User>(
        "https://api.spotify.com/v1/me",
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      return response.data
}

export async function fetchTopTracks(token:string) {
  const response =  await axios.get<responseTopTracks>(
      "https://api.spotify.com/v1/me/top/tracks?limit=10&offset=0",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    return response.data
}